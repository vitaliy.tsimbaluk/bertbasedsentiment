import axios from 'axios';
import Vue from 'vue'


const state = {
  sentiment: '',
  probability: null,
  inferenceTime: 0
};

const getters = {
  sentiment: state => {
    return state.sentiment;
  },
  probability: state => {
    return state.probability;
  },
  inferenceTime: state => {
    return state.inferenceTime;
  }
};

const actions = {
  async get({commit}, {text}) {
    // add authentification later
    let response = await axios.post('/api/sentiment',
      text,
      {
        headers: {
          'Content-Type' : 'text/plain'
        }
      });
    commit('setSentiment', response.data.sentiment);
    commit('setProbability', response.data.probability);
    commit('setInferenceTime', response.data.inferenceTime);
  }
};

const mutations = {
  setSentiment(state, value) {
    Vue.set(state, 'sentiment', value);
  },
  setProbability(state, value) {
    Vue.set(state, 'probability', value.toFixed(2));
  },
  setInferenceTime(state, value) {
    Vue.set(state, 'inferenceTime', value);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
