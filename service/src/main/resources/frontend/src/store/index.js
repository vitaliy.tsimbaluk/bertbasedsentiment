import Vue from 'vue';
import Vuex from 'vuex';
import sentiment from './modules/sentiment'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    sentiment
  }
});
