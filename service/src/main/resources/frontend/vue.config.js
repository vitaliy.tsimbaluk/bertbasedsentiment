module.exports = {
    // proxy all webpack dev-server requests starting with /api
    // see https://cli.vuejs.org/config/#devserver-proxy
    devServer: {
        proxy: {
            '/api': {
                target: 'http://localhost:8082',
                ws: true,
                changeOrigin: true
            }
        }
    },
};
