package com.nix.ml.sentiment.service;

import lombok.Data;

import java.util.List;

@Data
public class SentimentResponce {
    private String sentiment;
    private Float probability;
    private double inferenceTime;

    public SentimentResponce(List<Float> probs, double inferenceTime) {
        if (probs.get(0) < probs.get(1)) {
            this.sentiment = "positive";
            this.probability = probs.get(1);
        } else {
            this.sentiment = "negative";
            this.probability = probs.get(0);
        }
        this.inferenceTime = inferenceTime;
    }
}
