package com.nix.ml.sentiment.service.tokenizer;

import java.util.Arrays;
import java.util.stream.Stream;

public abstract class Tokenizer {
    protected static Stream<String> whitespaceTokenize(final String sequence) {
        return Arrays.stream(sequence.trim().split("\\s+"));
    }

    public abstract String[] tokenize(String sequence);

    public abstract String[][] tokenize(String... sequences);
}
