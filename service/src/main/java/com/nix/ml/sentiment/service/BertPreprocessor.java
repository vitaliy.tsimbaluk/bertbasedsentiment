package com.nix.ml.sentiment.service;

import com.nix.ml.sentiment.service.tokenizer.FullTokenizer;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class BertPreprocessor {
    private static final String SEPARATOR_TOKEN = "[SEP]";
    private static final String START_TOKEN = "[CLS]";
    private final Map<String, Integer> vocabulary;
    private final FullTokenizer tokenizer;
    private final int maxSequenceLength;
    private final int separatorTokenId;
    private final int startTokenId;

    private static class Inputs {
        private final List<Float> input, mask;

        public Inputs(final List<Float> inputIds, final List<Float> inputMask) {
            this.input = inputIds;
            this.mask = inputMask;
        }
    }

    public BertPreprocessor(final InputStream vocabulary, final int maxSequenceLength, final boolean doLowerCase) {
        this.vocabulary = loadVocabulary(vocabulary);
        this.tokenizer = new FullTokenizer(this.vocabulary, doLowerCase);
        this.maxSequenceLength = maxSequenceLength;
        final int[] ids = tokenizer.convert(new String[]{START_TOKEN, SEPARATOR_TOKEN});
        this.startTokenId = ids[0];
        this.separatorTokenId = ids[1];
    }

    public Map<String, List<Float>> getData(final String sequence) {
        Inputs inputs = getInputs(sequence);
        var result = new HashMap<String, List<Float>>();
        result.put("Input-Token", inputs.input);
        result.put("Input-Segment", inputs.mask);
        return result;
    }

    private Inputs getInputs(final String sequence) {
        final String[] tokens = tokenizer.tokenize(sequence);

        final var inputIds = new ArrayList<Float>();
        final var inputMask = new ArrayList<Float>();

        /*
         * In BERT:
         * inputIds are the indexes in the vocabulary for each token in the sequence
         * inputMask is a binary mask that shows which inputIds have valid data in them
         */
        final int[] ids = tokenizer.convert(tokens);
        inputIds.add((float) startTokenId);
        inputMask.add((float) 1);
        for (int i = 0; i < ids.length && i < maxSequenceLength - 2; i++) {
            inputIds.add((float) ids[i]);
            inputMask.add((float) 1);
        }
        inputIds.add((float) separatorTokenId);
        inputMask.add((float) 1);

        int zeros = maxSequenceLength - ids.length - 2;
        for (int i = 0; i < zeros; i++) {
            inputIds.add((float) 0);
            inputMask.add((float) 0);
        }

        return new Inputs(inputIds, inputMask);
    }

    private static Map<String, Integer> loadVocabulary(final InputStream vocabulary) {
        final Map<String, Integer> result = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(vocabulary))) {
            AtomicInteger index = new AtomicInteger();
            reader.lines().forEach(x -> result.put(x, index.getAndIncrement()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result;
    }
}
