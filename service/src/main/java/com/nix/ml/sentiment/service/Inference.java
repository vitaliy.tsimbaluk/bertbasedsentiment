package com.nix.ml.sentiment.service;

import com.google.protobuf.Int64Value;
import io.grpc.ManagedChannelBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.tensorflow.framework.DataType;
import org.tensorflow.framework.TensorProto;
import org.tensorflow.framework.TensorShapeProto;
import tensorflow.serving.Model;
import tensorflow.serving.Predict.PredictRequest;
import tensorflow.serving.PredictionServiceGrpc;

import java.io.IOException;

@RestController
public class Inference {
    private final String modelName;
    private final String host;
    private final int port;
    private final int modelVersion;
    private final String signature;
    private final int maxSequenceLength;
    private final BertPreprocessor tokenizer;

    @Autowired
    public Inference(
            @Value("classpath:vocab.txt") Resource resourceFile,
            @Value("${bert.serving.model}") String modelName,
            @Value("${bert.serving.host}") String host,
            @Value("${bert.serving.port}") int port,
            @Value("${bert.serving.version}") int modelVersion,
            @Value("${bert.serving.signature}") String signature,
            @Value("${bert.input.maxSequenceLength}") int maxSequenceLength) throws IOException {
        this.modelName = modelName;
        this.host = host;
        this.port = port;
        this.modelVersion = modelVersion;
        this.signature = signature;
        this.maxSequenceLength = maxSequenceLength;
        this.tokenizer = new BertPreprocessor(resourceFile.getInputStream(), maxSequenceLength, true);
    }

    @PostMapping("/api/sentiment")
    public SentimentResponce getSentiment(@RequestBody String text) {
        var data = tokenizer.getData(text);

        // create a channel
        var channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();
        var stub = PredictionServiceGrpc.newBlockingStub(channel);

        // create a modelspec
        var modelSpecBuilder = Model.ModelSpec.newBuilder();
        modelSpecBuilder.setName(modelName);
        modelSpecBuilder.setVersion(Int64Value.of(modelVersion));
        modelSpecBuilder.setSignatureName(signature);

        var predictRequestBuilder = PredictRequest.newBuilder();
        predictRequestBuilder.setModelSpec(modelSpecBuilder);

        data.forEach((key, values) -> {
            var tensorProtoBuilder = TensorProto.newBuilder();
            tensorProtoBuilder.setDtype(DataType.DT_FLOAT);

            var tensorShapeBuilder = TensorShapeProto.newBuilder();
            tensorProtoBuilder.setTensorShape(tensorShapeBuilder.build());
            values.forEach(x -> tensorProtoBuilder.addFloatVal(x));

            var batchSize = TensorShapeProto.Dim.newBuilder().setSize(1).build();
            var maxSequenceLength = TensorShapeProto.Dim.newBuilder().setSize(this.maxSequenceLength).build();
            var featuresShape = TensorShapeProto.newBuilder().addDim(batchSize).addDim(maxSequenceLength).build();
            tensorProtoBuilder.setTensorShape(featuresShape);

            var tensorProto = tensorProtoBuilder.build();

            predictRequestBuilder.putInputs(key, tensorProto);
        });

        var request = predictRequestBuilder.build();
        var inferenceTime = System.nanoTime();
        var response = stub.predict(request);
        inferenceTime = System.nanoTime() - inferenceTime;

        var sentiment = response.getOutputsMap().get("dense").getFloatValList();
        return new SentimentResponce(sentiment, inferenceTime / 1_000_000_000d);
    }
}
