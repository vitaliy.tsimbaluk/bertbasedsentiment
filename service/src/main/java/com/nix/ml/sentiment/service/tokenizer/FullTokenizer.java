package com.nix.ml.sentiment.service.tokenizer;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Stream;

public class FullTokenizer extends Tokenizer {
    private final BasicTokenizer basic;
    private final Map<String, Integer> vocabulary;
    private final WordpieceTokenizer wordpiece;

    public FullTokenizer(final Map<String, Integer> vocabulary, final boolean doLowerCase) {
        this.vocabulary = vocabulary;
        basic = new BasicTokenizer(doLowerCase);
        wordpiece = new WordpieceTokenizer(vocabulary);
    }

    public int[] convert(final String[] tokens) {
        return Arrays.stream(tokens).mapToInt(vocabulary::get).toArray();
    }

    @Override
    public String[] tokenize(final String sequence) {
        return Arrays.stream(wordpiece.tokenize(basic.tokenize(sequence)))
                .flatMap(Stream::of)
                .toArray(String[]::new);
    }

    @Override
    public String[][] tokenize(final String... sequences) {
        return Arrays.stream(basic.tokenize(sequences))
                .map((final String[] tokens) -> Arrays.stream(wordpiece.tokenize(tokens))
                        .flatMap(Stream::of)
                        .toArray(String[]::new))
                .toArray(String[][]::new);
    }
}
